# Nintendo Switch Setup Instructions

## Install Hekate bootloader

1. Extract ``hekate_ctcaer_5.5.8_Nyx_1.0.5.zip`` to ``sdcard:\``
2. Delete ``/bootloader/sys/nyx.bin`` (due to hekate hangs during RCM boot)
3. Create ``sdcard:\bootloader\hekate_ipl.ini`` from existing sources (e.g. [Homebrew SD Setup](https://sdsetup.com/))
4. Copy the ``hekate_ctcaer_5.1.1.bin`` to [Rekado](https://github.com/MenosGrante/Rekado)

## Install Atmosphere

1. Extract ``atmosphere-0.19.5-master-9df13781c+hbl-2.4.1+hbmenu-3.4.0.zip`` to ``sdcard:\``
2. Delete ``sdcard:\atmosphere\reboot_payload.bin``
3. Copy Atmosphere release page ``fusee-primary.bin`` to ``sdcard:\bootloader\payloads``

## Install sigpatches / nosigchk

1. Get _sigpatches_ from [gbatemp.net sigpatches thread](https://gbatemp.net/threads/sigpatches-for-atmosphere-hekate-fss0-fusee-primary-fusee-secondary.571543/)
2. Extract ``Hekate+AMS-fss0-sigpatches-19.5-cfw-12.1.0.zip\atmosphere`` to ``sdcard:\atmosphere``
3. Copy ``Hekate+AMS-fss0-sigpatches-19.5-cfw-12.1.0.zip\bootloader\patches.ini`` to ``sdcard:\bootloader``

## Install Awoo Installer

1. Get _Awoo-Installer.zip_ file from [github/Awoo-Installer releases](https://github.com/Huntereb/Awoo-Installer/releases)
2. Extract the zip to ``sdcard:`` such that the ``Awoo-Installer.nro`` file is located at ``sdcard:\switch\Awoo-Installer\Awoo-Installer.nro`` 

## Checkpoint

1. Get _Checkpoint.nro_ from [github/FlagBrew/Checkpoint releases](https://github.com/FlagBrew/Checkpoint/releases)
2. Put the file to ``sdcard:\switch``

## References
* [Hekate Releases](https://github.com/CTCaer/hekate/releases)
* [Hekate nosigchk Releases](https://github.com/Joonie86/hekate/releases)
* [Hekate configurations](https://github.com/CTCaer/hekate)
* [Atmosphere Releases](https://github.com/Atmosphere-NX/Atmosphere/releases)
* [Goldleaf](https://github.com/XorTroll/Goldleaf)